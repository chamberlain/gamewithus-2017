﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class CartBehaviour : MonoBehaviour
{
    public float Speed = 0.1F;

    public BoxCollider InnerTrigger;

    private Rigidbody Rigidbody;

    private GameMaster Game;

    void Start()
    {
        Rigidbody = GetComponent<Rigidbody>();
        Game = FindObjectOfType<GameMaster>();
    }

    void Update()
    {
        var position = transform.position;
        position.z += Speed * 0.1F;

        // 
        // transform.position = position;
        Rigidbody.velocity = new Vector3( 0, 0, Speed * 5F );
    }

    private void OnTriggerEnter( Collider other )
    {
        // Item gained
        var item = other.GetComponent<ItemBehaviour>();
        if( item == null ) return;
        else Game.GainItem( item );
    }

    private void OnTriggerExit( Collider other )
    {
        // Item lost
        var item = other.GetComponent<ItemBehaviour>();
        if( item == null ) return;
        else Game.LostItem( item );
    }
}
