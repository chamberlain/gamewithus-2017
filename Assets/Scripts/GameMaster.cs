﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using Random = UnityEngine.Random;

[RequireComponent( typeof( AudioSource ) )]
public class GameMaster : MonoBehaviour
{
    public AudioClip[] MusicSet;
    private int SongChoice = 0;

    public GameObject ShelfPrefab;

    public CartBehaviour Cart;

    public Transform FloorTransform;

    private int CartRow;
    private int TimeRemaining = 20;
    private int Level = 0;

    public ShelfInventory[] InventoryOptions;

    private AudioSource AudioSource;

    private Coroutine FadeRoutine;

    private Canvas Canvas;
    private Text MessageText;
    private Text ShoppingListText;
    private Text TimerText;

    private List<ShoppingGoal> ShoppingList;
    private List<ItemBehaviour> CartInventory;

    void Start()
    {
        // 
        AudioSource = GetComponent<AudioSource>();

        // 
        Canvas = GetComponentInChildren<Canvas>();
        TimerText = Canvas.transform.Find( "Timer" ).GetComponent<Text>();
        MessageText = Canvas.transform.Find( "Message" ).GetComponent<Text>();
        ShoppingListText = Canvas.transform.Find( "Shopping List Frame/Shopping List" ).GetComponent<Text>();

        //
        CartInventory = new List<ItemBehaviour>();
        ShoppingList = new List<ShoppingGoal>();
        ResetDifficulty();

        // 
        for( int i = 0; i < 15; i++ )
        {
            CreateShelf( new Vector3( +2, 0, i * 2 ), Quaternion.AngleAxis( 0, Vector3.forward ) );
            CreateShelf( new Vector3( -2, 0, i * 2 ), Quaternion.AngleAxis( 180, Vector3.forward ) );
        }

        // 
        InvokeRepeating( "OneSecondTick", 1F, 1F );

        // 
        SongChoice = Random.Range( 0, MusicSet.Length );
        ChangeMusic( SongChoice );
    }

    private void ResetDifficulty()
    {
        ClearCart();

        TimeRemaining = 20;
        TimerText.text = string.Format( "Time: {0}", TimeRemaining );
        Cart.Speed = 0.1F;
        Level = 0;

        GenerateShoppingList();
    }

    private void OneSecondTick()
    {
        TimeRemaining--;
        if( TimeRemaining < 0 )
        {
            ShowMessage( "Game Over!", Color.red );
            ResetDifficulty();
        }

        // 
        TimerText.text = string.Format( "Time: {0}", TimeRemaining );
    }

    private void CreateShelf( Vector3 pos, Quaternion rot )
    {
        // 
        var invDef = InventoryOptions[Random.Range( 0, InventoryOptions.Length )];
        var invObj = Instantiate( ShelfPrefab );

        //
        var inv_ShelfBehaviour = invObj.GetComponent<ShelfBehaviour>();
        inv_ShelfBehaviour.Inventory = invDef;

        // 
        var inv_Transform = invObj.GetComponent<Transform>();
        inv_Transform.rotation *= rot;
        inv_Transform.position = pos;
    }

    /// <summary>
    /// Changes the music
    /// </summary>
    public void ChangeMusic( int index )
    {
        var clip = MusicSet[index];
        SongChoice = index;

        // 
        AudioSource.clip = clip;
        AudioSource.Play();

        // 
        CancelInvoke( "MusicEndEvent" );
        Invoke( "MusicEndEvent", clip.length );
    }

    void MusicEndEvent()
    {
        var nextChoice = SongChoice + 1;
        if( nextChoice > MusicSet.Length )
            nextChoice = 0;

        ChangeMusic( nextChoice );
    }

    public void GenerateShoppingList()
    {
        //
        ShoppingList.Clear();

        //
        var itemList = new List<ShelfItem>( InventoryOptions.SelectMany( inv => inv.Items ).Distinct() );

        var listLength = Random.Range( 2, Mathf.Min( Level, itemList.Count ) );
        for( int i = 0; i < listLength; i++ )
        {
            var idx = Random.Range( 0, itemList.Count );
            var item = itemList[idx];
            itemList.RemoveAt( idx );

            // 
            ShoppingList.Add( new ShoppingGoal( item, Random.Range( 1, Level * 2 ) ) );
        }

        UpdateShoppingList();
    }

    public void UpdateShoppingList()
    {
        // 
        var listText = "Level: " + Level + "\n\n";

        foreach( var item in ShoppingList )
            listText += string.Format( "{2}/{0} x '{1}'\n", item.Count, item.Item.name, CountItem( item.Item ) );
        ShoppingListText.text = listText.Trim();
    }

    private void ShowMessage( string message, Color color )
    {
        MessageText.text = message;
        MessageText.color = color;

        // 
        var endColor = new Color( color.r, color.g, color.b, 0F );
        if( FadeRoutine != null ) StopCoroutine( FadeRoutine );
        FadeRoutine = StartCoroutine( FadeMessageText( 2.5F, color, endColor ) );
    }

    private IEnumerator FadeMessageText( float duration, Color startColor, Color endColor )
    {
        float start = Time.time;
        float elapsed = 0;
        while( elapsed < duration )
        {
            // calculate how far through we are
            elapsed = Time.time - start;
            float normalisedTime = Mathf.Clamp( elapsed / duration, 0, 1 );
            MessageText.color = Color.Lerp( startColor, endColor, normalisedTime );
            // wait for the next frame
            yield return null;
        }
    }

    internal void GainItem( ItemBehaviour item )
    {
        ShowMessage( string.Format( "Gained '{0}'", item.ItemDefinition.name ), Color.green );
        CartInventory.Add( item );

        // 
        if( IsOnTheList( item.ItemDefinition ) )
        {
            //
            if( CheckForCompleteList() )
            {
                ShowMessage( "Completed List!", Color.yellow );

                ClearCart();

                // Speed up cart 
                TimeRemaining += ( Level + 1 ) * 10;
                Cart.Speed *= 1.5F;
                Level++;

                // 
                GenerateShoppingList();
            }

            // 
            UpdateShoppingList();
        }
        else
        {
            ShowMessage( "Unwanted Item '" + item.ItemDefinition.name + "'", Color.red );

            // Blast Off! 
            var rb = item.GetComponent<Rigidbody>();
            var x = Random.Range( -1F, +1F );
            var y = Random.Range( 0F, +5F );
            var z = Random.Range( -1F, +1F );
            rb.AddForce( new Vector3( x, y, z ) * 500F );

            // Blast Off!
            foreach( var obj in CartInventory )
            {
                rb = obj.GetComponent<Rigidbody>();

                x = Random.Range( -1F, +1F );
                y = Random.Range( 0F, +3F );
                z = Random.Range( -1F, +1F );

                rb.AddForce( new Vector3( x, y, z ) * 200F );
            }
        }
    }

    internal void LostItem( ItemBehaviour item )
    {
        CartInventory.Remove( item );

        if( IsOnTheList( item.ItemDefinition ) )
        {
            ShowMessage( string.Format( "Lost '{0}'", item.ItemDefinition.name ), Color.red );
            UpdateShoppingList();
        }
    }

    void ClearCart()
    {
        // Clear out cart
        foreach( var inv in CartInventory )
            Destroy( inv.gameObject );
        CartInventory.Clear();
    }

    internal bool CheckForCompleteList()
    {
        // Check for each item in list 
        foreach( var goal in ShoppingList )
        {
            var itemCount = CountItem( goal.Item );
            if( itemCount < goal.Count )
                return false;
        }

        return true;
    }

    internal bool IsOnTheList( ShelfItem itemDef )
    {
        var found = false;
        foreach( var goal in ShoppingList )
        {
            // On the list
            if( goal.Item.name == itemDef.name )
            {
                found = true;
                break;
            }
        }

        return found;
    }

    internal int CountItem( ShelfItem itemDef )
    {
        return CartInventory.Where(
            x => x.ItemDefinition.name == itemDef.name ).Count();
    }

    void Update()
    {
        var cartRow = Mathf.RoundToInt( Cart.transform.position.z / ( 2F ) );
        if( cartRow != CartRow )
        {
            // 
            CartRow = cartRow;

            //  
            CreateShelf( new Vector3( +2, 0, ( CartRow + 15 - 1 ) * 2 ), Quaternion.AngleAxis( 0, Vector3.forward ) );
            CreateShelf( new Vector3( -2, 0, ( CartRow + 15 - 1 ) * 2 ), Quaternion.AngleAxis( 180, Vector3.forward ) );

            // 
            var floorPos = FloorTransform.position;
            floorPos.z += 2;
            FloorTransform.position = floorPos;
        }
    }

    public struct ShoppingGoal
    {
        public readonly ShelfItem Item;
        public readonly int Count;

        public ShoppingGoal( ShelfItem item, int count )
        {
            Item = item;
            Count = count;
        }
    }
}
