﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ShelfBehaviour : MonoBehaviour
{
    public ShelfInventory Inventory;

    public Transform UpperShelf;
    public Transform LowerShelf;

    private GameMaster Game;

    void Start()
    {
        // 
        Debug.Assert( Inventory != null, "Shelf must have an inventory." );
        Game = FindObjectOfType<GameMaster>();

        // 
        PopulateShelf( UpperShelf, Inventory.UpperInventory );
        PopulateShelf( LowerShelf, Inventory.LowerInventory );
    }

    void PopulateShelf( Transform shelfTransform, List<ShelfItem> inventory )
    {
        // Find all item slots
        var itemTransforms = shelfTransform.GetComponentsInChildren<Transform>();
        itemTransforms = itemTransforms.Where( x => x.name.StartsWith( "Item Location" ) ).Distinct().ToArray();

        // Assign random items to shelf
        foreach( var slot_Transform in itemTransforms )
        {
            // Choose a random item and instantiate it
            var itemDef = inventory[Random.Range( 0, inventory.Count )];
            var item = Instantiate( itemDef.Prefab, slot_Transform, false );

            // 
            var itemBehaviour = item.GetComponent<ItemBehaviour>();
            itemBehaviour.ItemDefinition = itemDef;

            // Assign material
            var meshRenderer = item.GetComponent<MeshRenderer>();
            meshRenderer.material = itemDef.Material;

            // Random rotate item?
            var angle = Random.Range( 0, 360 );
            item.transform.rotation *= Quaternion.AngleAxis( angle, Vector3.forward );
            item.transform.position += Vector3.up * 0.01F;

            // 
            slot_Transform.DetachChildren();
        }
    }
}
