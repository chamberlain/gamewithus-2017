﻿using System;
using UnityEngine;

public static class EnumUtil
{
    public static TEnum GetRandom<TEnum>()
    {
        EnsureEnum<TEnum>(); 
        var values = GetValues<TEnum>();
        return values[UnityEngine.Random.Range( 0, values.Length )];
    }

    public static TEnum[] GetValues<TEnum>()
    {
        EnsureEnum<TEnum>();
        return (TEnum[]) Enum.GetValues( typeof( TEnum ) );
    }

    private static void EnsureEnum<TEnum>()
    {
        var type = typeof( TEnum );
        Debug.AssertFormat( type.IsEnum, "Type {0}, is not an enum." );
    }
}