﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ReadOnlyAttribute : PropertyAttribute
{
    public readonly bool OnlyReadOnlyDuringPlay;

    public ReadOnlyAttribute( bool onlyReadOnlyDuringPlay = false )
    {
        OnlyReadOnlyDuringPlay = onlyReadOnlyDuringPlay;
    }
}

#if UNITY_EDITOR
[CustomPropertyDrawer( typeof( ReadOnlyAttribute ) )]
public class ReadOnlyDrawer : PropertyDrawer
{
    public override float GetPropertyHeight( SerializedProperty property,
                                            GUIContent label )
    {
        return EditorGUI.GetPropertyHeight( property, label, true );
    }

    public override void OnGUI( Rect position,
                               SerializedProperty property,
                               GUIContent label )
    {
        var attr = attribute as ReadOnlyAttribute;

        GUI.enabled = false;
        if( attr.OnlyReadOnlyDuringPlay )
            GUI.enabled = !Application.isPlaying;

        EditorGUI.PropertyField( position, property, label, true );
        GUI.enabled = true;
    }
}
#endif 
