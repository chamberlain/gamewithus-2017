﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using System.ComponentModel;

#if UNITY_EDITOR
using UnityEditor;
#endif

/// <summary>
/// Allows the storage of basic unity serializable data as a dictionary indexed by enum types.
/// </summary>
/// <typeparam name="TEnum"> Must be an enum. </typeparam>
/// <typeparam name="TValue"> Must be serializable by unity. </typeparam>
public abstract class EnumCollection<TEnum, TValue> : EnumCollection
{
    private TEnum[] EnumValues;

    public IEnumerable<TEnum> Keys { get { return EnumValues; } }

    public EnumCollection()
        : base( typeof( TEnum ), typeof( TValue ) )
    { }

    private void OnEnable()
    {
        EnumValues = (TEnum[]) Enum.GetValues( typeof( TEnum ) );
    }

    public KeyValuePair<TEnum, TValue> Find( TValue value )
    {
        var key = (TEnum) GetKey( value );
        return new KeyValuePair<TEnum, TValue>( key, value );
    }

    public TValue this[TEnum key]
    {
        get { return Get( key ); }
    }

    public TValue Get( TEnum key )
    {
        return (TValue) GetValue( key );
    }
}

/// <summary>
/// Allows the storage of basic unity serializable data as a dictionary indexed by enum types. <para/>
/// Note: Extend the generic version!
/// </summary>
public abstract class EnumCollection : ScriptableObject, ISerializationCallbackReceiver
{
    protected Type EnumType;
    protected Type ObjectType;

    [SerializeField]
    protected string EnumTypeName;

    [SerializeField]
    protected string ObjectTypeName;

    [SerializeField]
    private List<Entry> Entries;

    internal EnumCollection( Type enumType, Type objectType )
    {
        EnumType = enumType;
        ObjectType = objectType;
    }

    private Entry FindEntryByKey( object key )
    {
        return Entries.Find( x => x.EnumValue == (int) key );
    }

    private Entry FindEntryByVal( object value )
    {
        return Entries.Find( x => Equals( x.Object, value ) );
    }

    protected object GetValue( object key )
    {
        var entry = FindEntryByKey( key );
        if( entry == null ) throw new KeyNotFoundException();
        else return entry.Object;
    }

    protected object GetKey( object value )
    {
        var entry = FindEntryByKey( value );
        if( entry == null ) throw new KeyNotFoundException();
        else return entry.EnumValue;
    }

#if UNITY_EDITOR
    protected virtual void OnInspectorGUI( object obj )
    {
        EditorGUILayout.LabelField( string.Format( "Unknown Inspector Type '{0}'", ObjectType ) );
    }
#else
    protected virtual void OnInspectorGUI( object obj )
    {
        // Nothing
    }
#endif

    private void OnEnable()
    {
        // Ensure enum type is an enum
        if( !EnumType.IsEnum ) throw new InvalidOperationException( string.Format( "EnumCollection type {0} is not an enum.", EnumType ) );
        // 
        else if( Entries == null || Entries.Count == 0 )
        {
            // 
            Entries = new List<Entry>();

            // 
            var values = Enum.GetValues( EnumType );
            foreach( int value in values )
                Entries.Add( new Entry( this, value ) );
        }

        foreach( var e in Entries )
            e.UpdateTypeNames( this );
    }

    public void OnBeforeSerialize()
    {
        // Store types as string
        EnumTypeName = EnumType.AssemblyQualifiedName;
        ObjectTypeName = ObjectType.AssemblyQualifiedName;
    }

    public void OnAfterDeserialize()
    {
        // Recover types from string
        EnumType = Type.GetType( EnumTypeName );
        ObjectType = Type.GetType( ObjectTypeName );

        // Update the entries to know these types
        foreach( var e in Entries )
            e.UpdateTypeNames( this );
    }

    [Serializable]
    private class Entry : ISerializationCallbackReceiver
    {
        public EnumCollection EnumCollection;
        public int EnumValue;

        public UnityEngine.Object UnityObject;
        public string SystemObjectJson;
        public object SystemObject;

        public object Object
        {
            get
            {
                if( string.IsNullOrEmpty( SystemObjectJson ) ) return UnityObject;
                else return SystemObject;
            }
        }

        public Entry( EnumCollection col, int enumValue )
        {
            EnumValue = enumValue;
            UpdateTypeNames( col );

            if( !col.ObjectType.IsSubclassOf( typeof( UnityEngine.Object ) ) )
                SystemObject = Activator.CreateInstance( col.ObjectType );
        }

        internal void UpdateTypeNames( EnumCollection col )
        {
            EnumCollection = col;
        }

        public void OnBeforeSerialize()
        {
            if( UnityObject == null && SystemObject != null )
            {
                if( EnumCollection.ObjectType.IsPrimitive )
                {
                    SystemObjectJson = SystemObject.ToString();
                }
                else
                {
                    SystemObjectJson = JsonUtility.ToJson( SystemObject );
                }
            }
        }

        public void OnAfterDeserialize()
        {
            if( SystemObjectJson == null ) SystemObjectJson = string.Empty;
            if( SystemObjectJson.Length > 0 )
            {
                var type = Type.GetType( EnumCollection.ObjectTypeName );
                if( EnumCollection.ObjectType.IsPrimitive )
                {
                    var converter = TypeDescriptor.GetConverter( type );
                    SystemObject = converter.ConvertFrom( SystemObjectJson );
                }
                else
                {
                    SystemObject = JsonUtility.FromJson( SystemObjectJson, type );
                }
            }
        }
    }

#if UNITY_EDITOR
    [CanEditMultipleObjects]
    [CustomEditor( typeof( EnumCollection ), true )]
    public class EnumCollectionEditor : Editor
    {
        bool fold = true;

        protected override void OnHeaderGUI()
        {
            fold = EditorGUILayout.InspectorTitlebar( fold, target );
        }

        public override void OnInspectorGUI()
        {
            if( !fold ) return;
            // DrawDefaultInspector();

            var obj = target as EnumCollection;
            if( obj == null ) return;

            EditorGUILayout.HelpBox( string.Format( "Collection of \"{0}\" indexed by \"{1}\".", obj.ObjectType, obj.EnumType ), MessageType.None );

            var values = (int[]) Enum.GetValues( obj.EnumType );

            // 
            var set1 = obj.Entries.Select( x => x.EnumValue ).ToArray();
            foreach( var invalid in set1.Except( values ) )
                obj.Entries.RemoveAll( x => x.EnumValue == invalid );

            // 
            foreach( int value in values )
            {
                var idx = obj.Entries.FindIndex( x => x.EnumValue == value );
                var name = ObjectNames.NicifyVariableName( Enum.GetName( obj.EnumType, value ) );

                var entry = idx >= 0 ? obj.Entries[idx] : default( Entry );

                // 
                if( idx < 0 )
                {
                    entry = new Entry( obj, value );
                    obj.Entries.Add( entry );
                }

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.PrefixLabel( name );

                // UnityEngine.Object 
                if( obj.ObjectType.IsSubclassOf( typeof( UnityEngine.Object ) ) )
                {
                    var old = entry.UnityObject;
                    var val = EditorGUILayout.ObjectField( old, obj.ObjectType, allowSceneObjects: true );
                    if( old != val ) entry.UnityObject = val;
                }
                else
                // UnityEngine.Color
                if( ( obj.ObjectType == typeof( Color ) ) ||
                    ( obj.ObjectType == typeof( Color32 ) ) )
                {
                    var old = Cast<Color>( entry.SystemObject );
                    var val = EditorGUILayout.ColorField( old );
                    if( old != val ) entry.SystemObject = val;
                }
                else
                // UnityEngine.Bounds
                if( ( obj.ObjectType == typeof( Bounds ) ) )
                {
                    var old = Cast<Bounds>( entry.SystemObject );
                    var val = EditorGUILayout.BoundsField( old );
                    if( old != val ) entry.SystemObject = val;
                }
                else
                // UnityEngine.Vector2
                if( ( obj.ObjectType == typeof( Vector2 ) ) )
                {
                    var old = Cast<Vector2>( entry.SystemObject );
                    var val = EditorGUILayout.Vector2Field( string.Empty, old );
                    if( old != val ) entry.SystemObject = val;
                }
                else
                // UnityEngine.Vector3
                if( ( obj.ObjectType == typeof( Vector3 ) ) )
                {
                    var old = Cast<Vector3>( entry.SystemObject );
                    var val = EditorGUILayout.Vector3Field( string.Empty, old );
                    if( old != val ) entry.SystemObject = val;
                }
                else
                // UnityEngine.Vector4
                if( ( obj.ObjectType == typeof( Vector4 ) ) )
                {
                    var old = Cast<Vector4>( entry.SystemObject );
                    var val = EditorGUILayout.Vector4Field( string.Empty, old );
                    if( old != val ) entry.SystemObject = val;
                }
                else
                // System.Single
                if( ( obj.ObjectType == typeof( Single ) ) )
                {
                    var old = Cast<Single>( entry.SystemObject );
                    var val = EditorGUILayout.FloatField( old );
                    if( old != val ) entry.SystemObject = val;
                }
                else
                // System.Double
                if( ( obj.ObjectType == typeof( Double ) ) )
                {
                    var old = Cast<Double>( entry.SystemObject );
                    var val = EditorGUILayout.DoubleField( old );
                    if( old != val ) entry.SystemObject = val;
                }
                else
                // System.Int32
                if( ( obj.ObjectType == typeof( Int32 ) ) )
                {
                    var old = Cast<Int32>( entry.SystemObject );
                    var val = EditorGUILayout.IntField( old );
                    if( old != val ) entry.SystemObject = val;
                }
                else
                // System.Int64
                if( ( obj.ObjectType == typeof( Int64 ) ) )
                {
                    var old = Cast<Int64>( entry.SystemObject );
                    var val = EditorGUILayout.LongField( old );
                    if( old != val ) entry.SystemObject = val;
                }
                else
                // System.Boolean
                if( ( obj.ObjectType == typeof( Boolean ) ) )
                {
                    var old = Cast<Boolean>( entry.SystemObject );
                    var val = EditorGUILayout.Toggle( old );
                    if( old != val ) entry.SystemObject = val;
                }
                else
                // Enum
                if( ( obj.ObjectType.IsEnum ) )
                {
                    var old = Cast<Enum>( entry.SystemObject );
                    var val = EditorGUILayout.EnumPopup( old );
                    if( old != val ) entry.SystemObject = val;
                }
                else
                // String
                if( obj.ObjectType == typeof( string ) )
                {
                    var old = Cast<string>( entry.SystemObject );
                    var val = EditorGUILayout.TextArea( old );
                    if( old != val ) entry.SystemObject = val;
                }
                else
                {
                    // 
                    EditorGUILayout.LabelField( string.Format( "Unknown Inspector Type" ) );
                    obj.OnInspectorGUI( entry.Object );
                }

                EditorGUILayout.EndHorizontal();
            }

            // 
            EditorUtility.SetDirty( obj );
        }

        private T Cast<T>( object @object )
        {
            if( @object == null ) return default( T );
            else return (T) @object;
        }
    }

#endif
}