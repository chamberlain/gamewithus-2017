﻿using UnityEngine;

// [ExecuteInEditMode]
public class FollowCamera : MonoBehaviour
{
    public Transform FollowTransform;

    public Transform TargetTransform;

    public float FollowDistance = 10F;
    public float VerticalOffset = 2F;

    void Start()
    {

    }

    void Update()
    {
        if( FollowTransform != null )
        {
            var position = FollowTransform.position;
            position += ( FollowTransform.rotation * new Vector3( 0, 0.75F, 1F ) * -FollowDistance ) + Vector3.up * VerticalOffset;
            // transform.position = Vector3.Lerp( transform.position, position, Time.deltaTime * 8F );
            transform.position = position; // Vector3.Lerp( transform.position, position, Time.deltaTime * 8F );

            // 
            if( TargetTransform != null ) transform.LookAt( TargetTransform );
            else transform.LookAt( TargetTransform.position + Vector3.up * 2.0F );
        }
    }
}
