﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutOfViewKiller : MonoBehaviour
{
    private GameMaster Game;

    private void Start()
    {
        Game = FindObjectOfType<GameMaster>();
        InvokeRepeating( "DeathTick", 2F, 1F );
    }

    void DeathTick()
    {
        var sz = transform.position.z;
        var cz = Game.Cart.transform.position.z;
        if( sz < cz - 3 ) Destroy( gameObject );
    }
}
