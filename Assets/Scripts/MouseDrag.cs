﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseDrag : MonoBehaviour
{
    public Camera Camera;

    [ReadOnly]
    public ItemBehaviour Selected;

    [ReadOnly]
    public Vector3 DragCoordinate;

    void Start()
    {

    }

    void Update()
    {
        var mouseRay = Camera.ScreenPointToRay( Input.mousePosition );

        // Mouse pressed
        if( Input.GetMouseButtonDown( 0 ) )
        {
            RaycastHit info;
            if( Physics.Raycast( mouseRay, out info ) )
            {
                var other = info.collider.gameObject;
                Selected = other.GetComponent<ItemBehaviour>();
                if( Selected != null )
                {
                    // 
                    DragCoordinate = info.point;

                    // 
                    var joint = other.AddComponent<SpringJoint>();
                    joint.anchor = info.transform.InverseTransformPoint( info.point );
                    joint.autoConfigureConnectedAnchor = false;
                    joint.connectedAnchor = info.point;
                    joint.spring = 333F;
                    joint.damper = 33F;

                    // 
                    var body = other.GetComponent<Rigidbody>();
                    // body.interpolation = RigidbodyInterpolation.Interpolate;
                    body.angularDrag = 1F;
                    body.drag = 1F;
                    
                    //
                    // UnityEditor.Selection.activeGameObject = Selected.gameObject;
                }
            }
        }

        // Mouse released
        if( Input.GetMouseButtonUp( 0 ) )
        {
            // 
            if( Selected != null )
            {
                // 
                var body = Selected.GetComponent<Rigidbody>();
                body.angularDrag = 0.1F;
                body.drag = 0F;

                Destroy( Selected.GetComponent<SpringJoint>() );
                Selected = null;
            }
        }

        //
        if( Selected != null )
        {
            var rigidbody = Selected.GetComponent<Rigidbody>();
            var joint = Selected.GetComponent<SpringJoint>();

            var xyz = mouseRay.GetPoint( 2.25F );

            DragCoordinate.x = xyz.x;
            DragCoordinate.y = xyz.y;

            DragCoordinate.z = Mathf.Lerp( DragCoordinate.z, xyz.z, 0.1F );

            // 
            joint.connectedAnchor = DragCoordinate;
        }
    }
}
