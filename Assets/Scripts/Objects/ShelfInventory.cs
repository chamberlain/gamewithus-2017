﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[CreateAssetMenu]
public class ShelfInventory : ScriptableObject
{
    public List<ShelfItem> UpperInventory;

    public List<ShelfItem> LowerInventory;

    public IEnumerable<ShelfItem> Items
    {
        get
        {
            return UpperInventory.Concat( LowerInventory ).Distinct();
        }
    }
}