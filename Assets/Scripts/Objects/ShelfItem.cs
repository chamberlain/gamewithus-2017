﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[CreateAssetMenu]
public class ShelfItem : ScriptableObject
{
    public string Name;

    public GameObject Prefab;

    public Material Material;

    public float Cost;
}
